
	<table>
		<thead>
			<tr>
				<th><Tenants></Tenants></th>
				<th><Address></Address></th>
				<th>Property Link</th>
			</tr>
		</thead>
		<tbody>
			@foreach($landlord->properties() as $property) 
				<tr>
					<td>{{$property->number_of_tenants}}</td>
					<td>{{$property->postcode}}</td>
					<td><a href="/property{{$property->id}}">See More</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
