<?php

use App\Landlord;
use Carbon\Carbon;
use Illuminate\Database\Seeder;


class LandlordsAndPropertiesSeeder extends Seeder
{

    private $conn;
    private $count;
    private $now;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->init();
        $this->conn->table('landlords')->truncate();
        $this->conn->table('properties')->truncate();


        for ($i = 0; $i <= 100; $i++) {
            $faker = Faker\Factory::create('en_GB');
            $identifier = str_pad(
                $i + 1,
                8,
                '0',
                STR_PAD_LEFT
            );
            $this->addLandlord(
                $faker->safeEmail,
                '1',
                $identifier
            );
            unset($faker);

        }

    }

    /**
     * Initialise some commonly used variables
     */
    private function init()
    {
        $this->now = Carbon::now()->format('Y-m-d H:i:s');
        $this->conn = \DB::connection(config('database.default'));
        $this->count = 0;
    }


    private function addLandlord($email, $password, $propIdentifier)
    {

        // We may be adding multiple properties for the same landlord
        $newLandlord = Landlord::where(
            'email',
            '=',
            $email
        )->first();
        if (!$newLandlord) {
            // Add the landlord row
            $this->conn->table('landlords')->insert(
                [
                    'email' => $email,
                    'password' => bcrypt($password),
                    'remember_token' => '',
                    'created_at' => $this->now,
                    'updated_at' => $this->now,
                ]
            );
            // Read the new landlord back to get its id
            $newLandlord = Landlord::where(
                'email',
                '=',
                $email
            )->first();
        }

        if (!$newLandlord || count($newLandlord) <= 0) {
            throw new \Exception('Could not find new landlord for email {$email}');
        }

        $this->addRandomAmountOfProperties(
            $newLandlord,
            $propIdentifier
        );
        $this->count += 1;

    }

    private function addRandomAmountOfProperties($newLandlord, $propIdentifier)
    {
        for ($numOfProperties = 0; $numOfProperties <= rand(0, 3); $numOfProperties++) {
            $faker = Faker\Factory::create('en_GB');
            $this->conn->table('properties')->insert(
                [
                    'landlord_id' => $newLandlord->id,
                    'identifier' => $propIdentifier,
                    'address_1' => $faker->buildingNumber . " " . $faker->streetName,
                    'address_2' => $faker->secondaryAddress,
                    'postcode' => $faker->postcode,
                    'town' => $faker->city,
                    'county' => $faker->county,
                    'rent' => $faker->randomFloat(
                        2,
                        150,
                        1500
                    ),
                    'number_of_tenants' => $faker->randomDigit,
                    'created_at' => $this->now,
                    'updated_at' => $this->now,
                ]
            );
            unset($faker);
        }
    }
}
