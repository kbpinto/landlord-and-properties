<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->text('identifier');
            $table->integer('landlord_id');
            $table->text('address_1');
            $table->text('address_2')->nullable();
            $table->text('address_3')->nullable();
            $table->text('address_4')->nullable();
            $table->text('postcode');
            $table->text('town');
            $table->text('county')->nullable();
            $table->float('rent');
            $table->integer('number_of_tenants');
            $table->dateTime('last_login_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
