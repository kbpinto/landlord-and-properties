<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Landlord extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relationship with Properties
     */
    public function properties() {
        return $this->hasMany('App\Property','landlord_id','id');
    }
}
