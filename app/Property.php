<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identifier', 
	'landlord_id', 
	'address_1',
	'address_2',
	'address_3',
	'address_4',
	'postcode',
	'county',
	'town',
	'rent',
	'number_of_tenants',
	'last_login_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Relationship with Landloards
     */
    public function landlords() {
    	return $this->belongsTo('App\Lanlord');
    }

}
