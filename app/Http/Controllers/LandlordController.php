<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Landlord;

class LandlordController extends Controller
{
	/**
	 * Redirect the Landlord with Property details;
	 */
	public function index() {
		$id = Auth::user();
		$landlord = Landlord::find($id);
		return view("landlord.index",['landlord',$landlord]);
	}
}
