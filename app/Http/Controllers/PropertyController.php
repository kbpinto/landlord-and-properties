<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Property;

class PropertyController extends Controller
{
	public function index(Request $request) {
		$id = $request->input('id');
		return view('property.index', ['property' => Property::findOrFail($id)]);
	}
}
