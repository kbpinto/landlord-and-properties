# Test Exercise - Landlords and properties
## Story
In our little village every property belongs to a few infamous landlords. These landlords wanted to keep an eye on their properties so they implemented a website, with a simple authentication and a dashboard to see the chosen property details. In order to keep their records safe, they password protected the system where they can check their tenants and the beloved monthly income(s). They appreciated the default features of the framework such as login throttling.

Unfortunately they couldn’t figure out how to extend the authentication with an additional step to log in against the chosen property only. 

## Workflow
1. Fork this repository
2. Run the migration with seed
3. Commit sqlite database as initial commit
4. Commit often and logically
5. Spend no more than 1-3 hours on this

## Objective
As an addition to the normal laravel authentication add a secondary step for users with more than one properties in the database.

## Conditions of Acceptance
* As a user I should not see the secondary step if I only have one property.
* As a user I should only be logged in if I input an identifier that matches one of my properties.
* As a user I should see an error message if I input an incorrect identifier.
* As a user I should only see the information from the property that I selected on the second authentication step.
* As a user I should have updated information on the dashboard (based on the datatable) about my last login date.

## Bonus
* As a user I should not be allowed to have more attempts to submit the second step authentication than the default number of normal login attempts.
* As a user I should see an error message when I submit with incorrect email address but without page reload. 
* As a user I should see an error message when I submit with property identifier but without page reload. 
